Time Taken:
28h install + look for commands

#Change resolution:
## https://wiki.archlinux.org/title/HiDPI#Xfce
xfconf-query -c xsettings -p /Gdk/WindowScalingFactor -s 2
xfconf-query -c xfwm4 -p /general/theme -s Default-xhdpi
#install eduroam:
## https://mp.uu.se/sv/web/info/stod/it-telefoni/it-servicedesk/network-on-campus/eduroam/ubuntu/cat
#run:
python3 '/home/marie/Téléchargements/eduroam-linux-Uppsala_universitet.py'
#set keyboard correctly:
##https://unix.stackexchange.com/questions/302111/how-can-i-create-a-new-keyboard-layout-for-xfce4

#install gedit
sudo apt-get install gedit
#edit make login automatic at start: open a terminal:
## source: https://stackoverflow.com/questions/66025217/how-can-i-enable-auto-login-into-debian-10-xfce-session
sudo nano /etc/lightdm/lightdm.conf
#and add 
[Seat:*]
autologin-user=$USER
autologin-user-timeout=0

# change the locals to english
https://serverfault.com/questions/54591/how-to-install-change-locale-on-debian

#change the auto login to true
https://forums.debian.net/viewtopic.php?p=736982

#move the lock panel down
https://forums.debian.net/viewtopic.php?p=736982

#install scp
sudo apt install -y openssh-client openssh-server

#install printer 
https://mp.uu.se/en/web/info/stod/it-telefoni/it-servicedesk/utskrift/kom-igang-med-eduprint/eduprint-linux


#restore firefox profiles:
https://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data

#Change the keybinding
https://unix.stackexchange.com/questions/271150/how-to-set-ctrlc-to-copy-ctrlv-to-paste-and-ctrlshiftc-to-kill-process-in-x


#To start on the Bios/UEFI of the University computers, model Eliteone HP
press F1 while start and interact with the bios

#Don't forget to put a little terminal quick launcher in your menu bar

#Install vim
sudo apt-get install vim

#Install git
sudo apt-get install git

#install ssh
sudo apt install -y openssh-client openssh-server

#install printer
https://mp.uu.se/en/web/info/stod/it-telefoni/it-servicedesk/utskrift/kom-igang-med-eduprint/eduprint-linux
#List the printers
lpstat -p

#install cups
sudo apt-get install cups

#print a document via command line
lp -d eduPrint-UU

#The printer crendentials are (OBS!: replace by the appropriate ID+password from UU):
USER\mardu123
PASSWORD_A
#create a shorcut in the start bar for gedit usefulcommand.sh
https://www.google.com/search?q=put+a+shortcut+to+a+file+on+xfce&client=firefox-b-e&ei=Rt5eYcCFFZKfrwTHzrBg&ved=0ahUKEwjAwJOZnbjzAhWSz4sKHUcnDAwQ4dUDCA0&uact=5&oq=put+a+shortcut+to+a+file+on+xfce&gs_lcp=Cgdnd3Mtd2l6EAM6BwgAEEcQsAM6DgguEIAEEMcBEKMCEJMCOgsILhCABBDHARDRAzoFCC4QgAQ6BQgAEIAEOgsILhCABBDHARCjAjoHCC4QQxCTAjoECAAQQzoLCC4QgAQQxwEQrwE6DQguEMcBEK8BEAoQkwI6BAguEAo6CAguEIAEEJMCOgQIABATOgoIABAWEAoQHhATOggIABAWEB4QEzoKCAAQDRAFEB4QEzoGCAAQFhAeSgQIQRgAUPMVWM3QAWDy0gFoAnACeACAAaQBiAHHGJIBBTIyLjEwmAEAoAEByAEIwAEB&sclient=gws-wiz

#install zoom
#go on the zoom website, download the .deb package
#then run this command:
sudo apt --fix-broken install ./zoom_amd64.deb 


#install wget
sudo apt-get install wget

#install sublime
https://www.sublimetext.com/docs/linux_repositories.html#apt

#Change keyboard layout to swedish
setxkbmap -layout se -option compose:caps

#change keyboard layout to us
setxkbmap -layout us 

# Install gparted
sudo apt-get update && apt-get install gparted
apt-get install gparted

# Mount my external hardrive ntfs partition:
sudo mount /dev/sda3 ~/mountpoint/

# Install camera
https://github.com/patjak/bcwc_pcie/wiki/Get-Started#devvideo-not-created

# install docker
https://docs.docker.com/engine/install/debian/

# If problem of installation for docker try this:
https://stackoverflow.com/questions/44678725/cannot-connect-to-the-docker-daemon-at-unix-var-run-docker-sock-is-the-docker
git clone https://github.com/patjak/facetimehd-firmware.git

# change principal icons names:
https://askubuntu.com/questions/60161/change-the-default-downloads-directory

# In vim to be able to use the copy/paste via CTR-Shift-C (SUPER-C in my case)
:set mouse=r
https://github.com/SpaceVim/SpaceVim/issues/695

#learn how to search replace ctrl+D in vim
https://www.youtube.com/watch?v=YwMgnmZNWXA
https://www.reddit.com/r/vim/comments/cod91w/vim_feature_similar_to_ctrld_in_vscode/

#install my dell monitor drivers
https://www.dell.com/support/home/fr-fr/product-support/product/dell-u4021qw-monitor/drivers

# set up a github token
https://github.com/settings/tokens

# to stop having the printer cups asking for the password:
https://askubuntu.com/questions/57493/save-print-server-authentication-credentials


# to change the screen display
Display>Advanced>Dell40
#to change the mouse acceleration:
Mouse and touchpad>Devices>Dell computer... laser mouse>pointer speed 10

# Remove element os
sudo apt-get remove "element-desktop"

#in R studio installing magrittr and tidyverse
https://community.rstudio.com/t/tidyverse-wont-install/18680

#install orgmode of emacs
https://orgmode.org/org.html#Installation

# remove the grouping windows from xfce
https://forum.xfce.org/viewtopic.php?id=7529

# to cut mp3
#install ffmpeg
 sudo apt install ffmpeg
 ffmpeg -i input.mp3 -vn -acodec copy -ss 00:00:00 -to 00:01:32 output.mp3
 https://askubuntu.com/questions/27574/how-can-i-split-a-mp3-file
 
 # set *.org as a file to be used with spacemacs:
 ## follow the procedure on this page: at the end make a xml file like the example and install it with the command 
 xdg-mime install orgthings-org.xml #change the xml name to adapt to your XML file you made!
 ## log in and out session so that xfce picks things up
 https://portland.freedesktop.org/doc/xdg-mime.html
 
# Wifi code tilo:
 XJhAUxx mercenaire dindons dindons
 
 
#check for which device has optimised power
sudo powertop #then tab to tunable

 #read a yaml yq
 snap run yq eval -P '/home/marie/Documents/wattpad/comments1000.json'
 
 #connect bluetooth
 https://www.reddit.com/r/debian/comments/lolhkf/bluetooth_with_xfce_not_working/
 https://askubuntu.com/questions/1115671/blueman-protocol-not-available
 https://askubuntu.com/questions/833322/pair-bose-quietcomfort-35-with-ubuntu-over-bluetooth
 
 
 #install calculator
 sudo apt-get install -y gnome-calculator
 
 
# change default gnumeric to .ods format
https://bugzilla.gnome.org/show_bug.cgi?id=345067
vim /usr/lib/gnumeric/1.12.48/plugins/openoffice/plugin.xml #modify this file as indicated in the link above

# install in2csv

#find a file
find . -iname "*.mp3" -print 2>/dev/null
 
 #create exfat partition(when gparted bug)
 sudo mkfs.exfat /dev/sdd3
 
 
 # for the run scummvm game le maitre des elements
 sudo chown -R marie .
 
 # for runing a command over the find files
 find . -iname '*.exe' -exec strings {} \; > allstrings.removeme
 
 #copy un dossier via Rsync on my usb without removing files from first argument
 sudo rsync -Pavr '/home/marie/.wineLeMaitreDesElements/drive_c/Jeux/ABWFR/LeMaitreDesElements' '/media/marie/SHARE/roms/windows/setup-01650-Le_Maitre_Des_Elements-PCWin.wine/drive_c/Jeux/ABWFR/LeMaitreDesElements' 
 
 #make a alt+tab auto
 xdotool keydown alt+Tab; xdotool keyup Tab; sleep 3; xdotool keyup alt
 
 xdotool keydown alt+Tab; xdotool keyup Tab; sleep 3; xdotool keyup alt
 xdotool keydown alt+Tab && xdotool keyup Tab && sleep 2 && xdotool keyup alt
 
 The Three-Finger Swipe Up
windows overview three fingers up "xfce" for alt+tab
https://fransdejonge.com/2018/05/mimic-windows-touchpad-gestures-in-xfce-with-libinput-gestures/
 
 
 
webcam install:
# follow this instruction:
https://github.com/patjak/facetimehd/wiki/Get-Started#devvideo-not-created
#and above all this instructions @debian :
https://github.com/patjak/facetimehd/wiki/Installation#get-started-on-debian

#Attention! Il faut adapter le linux header comme ceci:
sudo apt-get install linux-headers-5.10.0-10-amd64 git kmod libssl-dev checkinstall
(change 5.10.0-10-amd64 en fonction de l output de uname -r)
Done. The new package has been installed and saved to

 /home/marie/Telechargements/facetimehd-firmware/bcwc_pcie/bcwc-pcie_20220124-1_amd64.deb

 You can remove it from your system anytime using: 

      dpkg -r bcwc-pcie
 
 #install latex
 sudo apt install texlive
 
 
#making ssh key good tutorial:
https://serverpilot.io/docs/how-to-use-ssh-public-key-authentication/
ssh-keygen
ssh-copy-id marie@stp.lingfil.uu.se
 
#Init a venv:
python3 -m venv .env
 #only the first time
. ./xml_pack/bin/activate #for scripts like render.sh or run.sh
#In commandline:
source xml_pack/bin/activate 
pip freeze > requirements.txt
pip install -r requirements.txt

#making a back up on the studentroom for the blue hd
rsync -Pavr --delete /home/marie/ marie@studentroom:/mnt/external/marie_blueHD/



 
 
 https://github.com/mardub1635/chiasmus-detector.git
 #How to push an existing repo
cd existing_folder
git init --initial-branch=master
git remote add origin https://gitlab.com/mardub/song.git
git add .
git commit -m "Initial commit"
git push -u origin master

#how to pus also to github
git remote add mirror https://github.com/CDHUppsala/epub_study
git push mirror master
Note to self: as a password: enter the personal token called "github debian personal token"


#make git credential stored
https://stackoverflow.com/questions/11506124/how-to-enter-command-with-password-for-git-pull
git config credential.helper store

#Bluetooth paring
bluetoothctl
power on
scan on
agent on
remove 88:C9:E8:88:81:CE
trust 88:C9:E8:88:81:CE
pair 88:C9:E8:88:81:CE
connect 88:C9:E8:88:81:CE # varier l'ordre above


